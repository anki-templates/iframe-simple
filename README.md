# Iframe simple

Parce que Anki peut aussi être très polyvalent !

Modèle de carte permettant d'afficher n'importe quel site internet au recto d'une carte Anki de façon responsive.

Vous pouvez ouvrir le fichier [exemple](/exemple.apkg) avec Anki pour ajouter le type de note `Iframe simple` et créer vos propres cartes avec ce type de note (il suffit pour cela de coller un lien dans le champ "Lien").

## Captures d'écran

### Desktop

|Recto|Verso|
|:---:|:---:|
|![](/images/desktop-recto.png)|![](/images/desktop-verso.png)

### AnkiMobile

#### Portrait

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-portrait-recto.png)|![](/images/ankimobile-portrait-verso.png)

#### Paysage

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-paysage-recto.png)|![](/images/ankimobile-paysage-verso.png)
